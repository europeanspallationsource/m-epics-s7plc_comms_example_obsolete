# Call the snippet responsible for configuring IOC to PLC comms configuration
requireSnippet(s7plc-comms.cmd, "PLCNAME=$(PLCNAME),IPADDR=$(IPADDR),S7DRVPORT=$(S7DRVPORT), MODBUSDRVPORT=$(MODBUSDRVPORT),INSIZE=$(INSIZE),OUTSIZE=$(OUTSIZE),BIGENDIAN=1,RECVTIMEOUT=$(RECVTIMEOUT),SENDINTERVAL=$(SENDINTERVAL)")

#Load the database defining your EPICS records
dbLoadRecords(example_s7plc.db, "PLCNAME = $(PLCNAME)")
dbLoadRecords(example_modbus.db)
